.POSIX:
CC = gcc
CXX = g++
MPICC = mpicc
MPICXX = mpicxx
CXXFLAGS = -std=c++17 -O3 -lm -pthread

.PHONY: all
all: hw1

hw1: hw1.cc
	$(MPICXX) -cxx=$(CXX) $(CXXFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -f hw1
