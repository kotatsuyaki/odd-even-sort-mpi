\documentclass[egregdoesnotlikesansseriftitles,12pt,a4paper]{scrartcl}

\usepackage[margin=2cm]{geometry}
\usepackage{xeCJK}
\usepackage{setspace}
\onehalfspacing

% figure packages
\usepackage{graphicx}
\usepackage{listings}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{textcomp}
\usepackage{float}

% math packages
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage[ruled]{algorithm2e}
\SetKwComment{Comment}{/* }{ */}

% ref packages
\usepackage{hyperref}
\usepackage{cleveref}

% tables
\usepackage{booktabs, siunitx}
\usepackage[dvipsnames,svgnames,table]{xcolor}

% svg
\usepackage{svg}

% font setup
\setCJKmainfont{Source Han Serif TC}
\usepackage{tgpagella}
\usepackage{inconsolata}
\usepackage[T1]{fontenc}

% appendix
\usepackage[toc,page]{appendix}

% math operators
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}

% "C++"
\newcommand\CC{C\nolinebreak\hspace{-.05em}\raisebox{.4ex}{\relsize{-3}{\textbf{+}}}\nolinebreak\hspace{-.10em}\raisebox{.4ex}{\relsize{-3}{\textbf{+}}}}

% colored links
\newcommand\myshade{85}
\newcommand\NZ{\mathbb{N}_0}

\colorlet{mylinkcolor}{violet}
\colorlet{mycitecolor}{YellowOrange}
\colorlet{myurlcolor}{Aquamarine}

\hypersetup{
	linkcolor  = mylinkcolor!\myshade!black,
	citecolor  = mycitecolor!\myshade!black,
	urlcolor   = myurlcolor!\myshade!black,
	colorlinks = true
}

% color whole reference word
\Crefformat{algorithm}{#2Algorithm~#1#3}
\crefformat{algorithm}{#2algorithm~#1#3}
\Crefformat{figure}{#2Figure~#1#3}
\crefformat{figure}{#2figure~#1#3}

% metadata
\title{Lab 1 Report: Odd-Even Sort}
\subtitle{CS5422 Parallel Programming}
\author{
	黃明瀧\\
	\texttt{107021129}
}

\begin{document}
\maketitle
\tableofcontents

\section{Implementation Details}
My implementation of odd-even transposition sort follows the directions given
in the book \textit{An Introduction to Parallel Programming} by Peter Pacheco.

\subsection{Handling Arbitrary Sizes}
Given an input array of length $n$ and $m$ processors (referred to as $p_1, \dots, p_m$ from now on),
$p_1, \dots, p_{m-1}$ reads non-overlapping consecutive $k = \ceil*{\frac n m}$ numbers,
and $p_m$ reads all the remaining ones.
To make programming easier, the trialing empty space on $p_m$ is filled with positive infinities.

For the edge case where $n < m$, a new MPI group and a communicator with size $n$ is created,
replacing the original communicator.
By doing so, there's exactly one element on each processor, which is okay for the sorting algorithm to work.

\subsection{Sorting Strategy}
\Cref{alg:odd-even-sort} gives an outline of the odd-even transposition sorting algorithm applied in my work.

\subsubsection{Local Radix Sort}
Prior to the odd-even sort phases, the workers sort their local items using an implementation of radix sort first.
The radix sort algorithm for IEEE754 binary32 numbers is adapted from an old blog post\footnote{\url{http://stereopsis.com/radix.html}} on the Internet,
where several tricks are applied to sort floating point numbers.
\begin{itemize}
	\item The sign bit is flipped before and after the sort,
	      since numbers with sign bit \verb|0| are actually grater greater than those with sign bit \verb|1|.
	\item
	      For negative numbers, the remaining exponent and mantissa bits are also flipped before and after the sort.
	      This follows from the observation that negative numbers with larger magnitudes are actually smaller.
	\item
	      $3$ lists of buckets with length $2048$ are used, which is chosen empirically (I tried $2$ or $4$ lists but it got slower).
	      In each pass, the list is sorted based on $11$ bits.
\end{itemize}
This radix sort algorithm performs better than \verb|std::sort| in the extreme cases
where the numbers of elements are huge.

\subsubsection{Exchange and Merge}
After the local radix sort, $m$ exchange-and-merge phases are performed.
A naive implementation works like this.
\begin{itemize}
	\item Send the whole buffer to the partner.
	\item
	      Retain the smallest (largest) $k$ elements by merging the local buffer and the received buffer.

	      The merging can be done by maintaining two iterators, comparing the values,
	      pushing the smaller (larger) one to a temporary array,
	      and advancing the iterator of the smaller (larger) side.
\end{itemize}

\subsubsection{Reducing Communication}
To reduce communication,
the \textit{maximal (minimal) element} $e_l$ ($e_r$) and the \textit{number of items promised to be kept} $k_l$ ($k_r$)
are exchanged before sending the actual buffers.
For example if we have $[1,3,5,7,9]$ and $[6,8,10,12,14]$ before the exchange on the two procs,
the left hand side sends only $[7,9]$, because
\begin{enumerate}
	\item $[1, 3, 5]$ are smaller than the partner's minimum value $6$, and because
	\item the partner already wants to keep $3$ values anyways.
\end{enumerate}
Similarly, the right hand side sends only $[6,8]$.
Refer to \cref{alg:odd-even-sort} for the details.

\subsubsection{Early Stopping}
The main exchange loop terminates when no elements are exchanged in any phase other than the first phase.
In some extreme cases (where the input array is already almost sorted),
this reduces the required time by a huge factor.

\subsubsection{Asynchronous Operations}
A lot of operations are implemented asynchronously in my code as an attempt to reduce waiting time.
For example, when exchanging the large buffers with the partner, the local ``kept'' elements are copied to the sorted buffer in the meantime.
The asynchronous tasks are wrapped as \verb|std::future<T>| types,
allowing them to be moved around before the \verb|.get()| or \verb|.wait()| operation.

Up to the time of writing, the following tasks are done asynchronously.
\begin{itemize}
	\item Sending and receiving operations with MPI.
	\item Allocation of large buffers.
\end{itemize}


\section{Experiments and Analyses}
\subsection{System Spec}
The following experiments are done on the cluster provided by the course,
with the following specs.
\begin{itemize}
	\item Intel X5670 2x6 cores @ 2.93GHz on each node.
	\item 96 GiB of memory on each node.
	\item Quad-data rate Infiniband network.
	\item Shared RAID5 disk.
\end{itemize}

\subsection{Time Measurement and Visualization}
To measure time,
\verb|std::chrono::steady_clock| from the standard library is used to obtain time values in microseconds.
Since there are a lot of overlapping asynchronous operations in the code, I opted to record and generate
log files in the
Trace Event Format\footnote{\url{https://docs.google.com/document/d/1CvAClvFfyA5R-PhYUmn5OOQtYMH4h6I0nSsKchNAySU/preview}}.
This file format can then be read and explored interactively using Chromium's built-in tracing tool available locally at \verb|chrome://tracing|.
\Cref{fig:tracing} is an example of how it looks.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{tracing-example.png}
	\caption{\label{fig:tracing}Example of me using Chromium's tracing tool}
\end{figure}

To calculate the total time spent on computation, communication, and I/O,
the durations of relevant time spans are simply added together.
\begin{itemize}
	\item Computation time = alloc + presort + precopy + merge
	\item I/O time = readfile + writefile
	\item Communication time = send(s) + recv(s) + exchange(s)
\end{itemize}
Notice that \verb|send_buf| is not waited immediately when it's done,
so it is excluded from the calculation.

\subsection{Experiment Results}
The testcase \verb|38.in| is used in the experiments.
The program is compiled once and run with the following configurations.
\begin{enumerate}
	\item $1$ node with $1$ process.
	\item $1$ node with $4$ processes.
	\item $2$ nodes, each with $4$ processes.
	\item $4$ nodes, each with $4$ processes.
\end{enumerate}

\subsubsection{Identifying the Bottleneck}
\begin{figure}[h]
	\centering
	\includesvg[width=0.6\textwidth]{times}
	\caption{\label{fig:cumultimes}Cumulative times across all cores}
\end{figure}

\begin{figure}[h]
	\centering
	\includesvg[width=0.6\textwidth]{normed}
	\caption{\label{fig:normed}Normlized times across all cores}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{4-4.png}
	\caption{\label{fig:writefile}Focus on the long ``writefile'' parts}
\end{figure}

\Cref{fig:cumultimes} shows the summation of runtime spent on different parts of the sorting process,
and \cref{fig:normed} shows the same figures but divided by the number of processes.
\begin{itemize}
	\item
	      The total time spent on computation remains pretty much the same, which is reasonable
	      since most of the computations (radix sort, merging, copying) have linear complexity w.r.t the problem size.
	\item
	      The cumulated communication time grows roughly proportional to number of processes,
	      while the normalized time remains more or less the same.
	      This is also reasonable, since the number of elements to be exchanged between processes does not vary a lot with the number of processes.
	      With more processes, we get less elements exchanged between two processes in every phase.
	\item
	      The IO time \textbf{grows dramatically} once we get from one node to multiple nodes.
\end{itemize}

Here is my claim: the IO time becomes the bottleneck as the number of processes goes up.
This is also highly evident from \cref{fig:writefile}, where the ``writefile'' parts span unreasonably long compared to other operations.
Either the problem lies in the MPIIO file write methods (which is out of my control)
or that file writing operations across multiple machines are inherently slow.

\subsubsection{Scalability}

\Cref{fig:total} and \cref{fig:speedup} shows the total runtimes and speedup factors, respectively.
The program scales poorly and far from the ideal speedup (the dotted blue line),
mostly because of the aforementioned problem of slow file writes with large number of processes.

\begin{figure}[h]
	\centering
	\includesvg[width=0.6\textwidth]{total}
	\caption{\label{fig:total}Total runtimes}
\end{figure}

\begin{figure}[h]
	\centering
	\includesvg[width=0.6\textwidth]{speedup}
	\caption{\label{fig:speedup}Speedup factors}
\end{figure}


\newpage
\section{Conclusion}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.35\textwidth]{leader.png}
	\caption{\label{fig:rank}I ranked the first place!}
\end{figure}

First of all, I had a lot of fun implementing this homework.
After trying several optimizations that I can think of,
I've ranked very close to the \verb|pp21t00|\footnote{Is this TA or teacher's code? I'm highly intereseted in how it achieved such remarkable performance.} with a total runtime of 131.86 seconds, on the public scoreboard.
During the process,
I learned to apply radix sort on floating point numbers,
to generate Trace Event Format files,
and to use the modern \verb|std::async| API, etc.

\subsection*{How about Other Languages?}
Had it not been for the \CC{} language restriction of this homework,
I probably would have chosen another programming language for the task.
The first thing that came to my mind is my favorite language Rust,
which seems like a good fit for solving several problems in my current codebase.
\begin{itemize}
	\item Concurrency in \CC{} is unneededly dangerous and requires too much care from the programmer.
	      I had very pleasant experience so far with Rust's async ecosystem.
	\item \CC{} doesn't have first-class support for ownership semantics.
	      I was responsible for ensuring that the buffers aren't being written to while it's still used as a send buffer.
	      Rust, on the other hand, may be able to model this simply by taking the ownership (i.e. exclusive reference) when the buffer is still being sent.
\end{itemize}
I am running out of time for this homework submission, but I am going to re-implement it in Rust for fun later in this semester, anyways.

\newpage
\begin{appendices}
	\section{Pseudocode listings}
	\begin{algorithm}
		\caption{The sorting algorithm}\label{alg:odd-even-sort}
		\DontPrintSemicolon
		\KwData{$A = [ a_1, a_2, \dots, a_k ]$ where $k = \ceil*{\frac n m}$}
		Sort $A$ locally\;

		\Repeat{$m$ phases elapsed or there's no more exchanges}{
			$p \gets$ my rank\;
			$q \gets$ partner's rank for this phase\;
			\eIf{$p > q$}{
				$e_r \gets \max(A)$\;
				$\textsc{Send}(e_r, q)$\;
				$e_l \gets \textsc{Recv}(q)$ \Comment*{threshold for items to be kept}

				$k_r \gets$ number of items in $A$ greater than $e_l$ \Comment*{num of items to keep}
				$\textsc{Send}(k_r, q)$\;
				$k_l \gets \textsc{Recv}(q)$\;

				$A_\text{keep} \gets$ last $k_r$ items in $A$\;
				$A_r \gets$ last $\min(k - k_r, k - k_l)$ items in $A$ \Comment*{send at most partner's need}

				$\textsc{Send}(A_r, q)$\;
				$A_l \gets \textsc{Recv}(q)$\;
				$A \gets k$ largest items in $A_\text{keep}$ and $A_l$, ascending\;
			}{
				$e_l \gets \max(A)$\;
				$\textsc{Send}(e_l, q)$\;
				$e_r \gets \textsc{Recv}(q)$ \Comment*{threshold for items to be kept}

				$k_l \gets$ number of items in $A$ less than $e_r$ \Comment*{num of items to keep}
				$\textsc{Send}(k_l, q)$\;
				$k_r \gets \textsc{Recv}(q)$\;

				$A_\text{keep} \gets$ first $k_l$ items in $A$\;
				$A_l \gets$ first $\min(k - k_l, k - k_r)$ items in $A$ \Comment*{send at most partner's need}

				$\textsc{Send}(A_l, q)$\;
				$A_r \gets \textsc{Recv}(q)$\;
				$A \gets k$ smallest items in $A_\text{keep}$ and $A_r$, ascending\;
			}
		}
	\end{algorithm}

\end{appendices}
\end{document}
