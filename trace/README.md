# Some useful commands

To convert raw output to trace event format:

```sh
cat head.txt log-2-4.txt tail.txt | hjson -j > log-2-4.json
```

To find out sum of merging time:

```sh
for file in log-1-1.json log-1-4.json log-2-4.json log-4-4.json; do
    cat $file | jq '.traceEvents | map(select(.name == "merge")) | map(.dur / 1000.0) | add'
done
```
